import React from 'react';
import { AppRegistry } from 'react-native';

// NavigationConfig for stackNavigation
import Navigator from './app/navigation'

AppRegistry.registerComponent('bluebooth', () => Navigator);