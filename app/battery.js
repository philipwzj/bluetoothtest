/* This is the battery page of the SensorCloud
 * In this page, the user could get the battery information by turning on the switch
 * The logic is very similar to the temperature page, which has been commented
 * Note: the UUID for android and iOS could be different
 *
 * by: Philip Wang
 * on: June 22nd, 2017
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    NativeAppEventEmitter,
    NativeEventEmitter,
    NativeModules,
    Platform,
    TouchableOpacity,
    Switch,
    Alert,
    Dimensions
} from 'react-native';
import BleManager from 'react-native-ble-manager';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const {width, height} = Dimensions.get('window');

var serviceIDbat = "180F";
var charIDbat = '2A19';

export default class Battery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            battery: NaN,
            switch: false,
            visible: false,
        };
    }

    componentDidMount() {
        bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', (data) => {
            if (Platform.OS !== 'ios') {
                charIDbat = "00002a19-0000-1000-8000-00805f9b34fb";
            }
            if (data.characteristic === charIDbat) {
                this.setState({battery: data.value[0]});
            }
        });
        
    }


    switchPressed = () => {
        if (this.state.switch === false) {
            this.setState({switch: true});
            this.startNotification();
        } else {
            this.setState({switch: false});
            this.stopNotification();
        }
    };

    startNotification() {
        BleManager.read(deviceID, serviceIDbat, charIDbat)
            .then((readData) => {
                // Success code
                console.log('Read: ' + readData);
                this.setState({battery: readData});
            })
            .catch((error) => {
                // Failure code
                console.log(error);
            });
        setTimeout(() => {
        BleManager.startNotification(deviceID, serviceIDbat, charIDbat)
            .then(() => {
                console.log('Notification started');
            })
            .catch((error) => {
                console.log('Notification error:', error);
            });
        }, 400);

    }

    stopNotification() {
        this.setState({temperature: NaN});
        BleManager.stopNotification(deviceID, serviceIDbat, charIDbat)
            .then(() => {
                console.log('stopNotification success!');
            })
            .catch((error) => {
                console.log('stopNotification error:', error);
            });

        this.setState({battery: NaN});
    }

    disconnectSensor() {
        BleManager.disconnect(deviceID)
            .then(() => {
                console.log('Disconnected');
            })
            .catch((error) => {
                console.log(error);
            });
    }

    connectSensor = () => {
        BleManager.connect(deviceID)
            .then(() => {
                BleManager.retrieveServices(deviceID)
                    .then((peripheralInfo) => {
                        console.log(peripheralInfo);
                    });
            })
            .catch((error) => {
                Alert.alert(
                    'ERROR',
                    'Device cannot be connect',
                    [
                        {text: 'Retry', onPress: () => this.connectSensor()},
                        {text: 'Rescan', onPress: () => this.startScan()},
                        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    ],
                    {cancelable: false}
                );
            });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.switchView}>
                    <Text style={styles.switchText}>Live Data</Text>
                    <Switch onValueChange={this.switchPressed} value={this.state.switch} style={{marginBottom: 10}}/>
                </View>

                <View>
                    <TouchableOpacity
                        style={[styles.button, styles.center]}
                        activeOpacity={0.7}
                        onPress={() => this.connectSensor()}>
                        <Text style={styles.text}>Connect</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.button, styles.center]}
                        activeOpacity={0.7}
                        onPress={() => this.disconnectSensor()}>
                        <Text style={styles.text}>Disconnect</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.tempView}>
                    <Text style={styles.tempText}>{this.state.battery}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: (Platform.OS === 'ios') ? 30 : 0,
        justifyContent: 'space-between',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    switchView: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: (Platform.OS === 'ios') ? 20 : 15,
        backgroundColor: 'rgb(212, 224, 206)'
    },
    switchText: {
        fontSize: 30,
    },
    tempView: {
        width: width,
        height: 120,
        backgroundColor: 'rgb(212, 224, 206)',
    },
    tempText: {
        textAlign: 'center',
        fontSize: 70,
        paddingTop: 20,
    },
    button: {
        height: 50,
        paddingLeft: 20,
        paddingRight: 20,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 5,
        marginTop: 20,
        backgroundColor: 'rgb(212, 224, 206)',
    },

});

