/* This is the LED page of the SensorCloud
 * In this page, the user could change the LED light color on the sensor, given a probe is connected
 * All the information sent to the probe is done by byte array, through a function conversion
 *
 * by: Philip Wang
 * on: June 22nd, 2017
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    NativeAppEventEmitter,
    NativeEventEmitter,
    NativeModules,
    TouchableOpacity,
    Dimensions,
    Platform
} from 'react-native';
import BleManager from 'react-native-ble-manager';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const {width, height} = Dimensions.get('window');

const serviceIDled = "9D55A001-BC06-40EC-94CB-266609000419";
const charIDled = '9D55C100-BC06-40EC-94CB-266609000419';

// Strings for LED colors
const RED = hexToBytes('1');
const GREEN = hexToBytes('2');
const YELLOW = hexToBytes('3');
const BLUE = hexToBytes('4');
const PURPLE = hexToBytes('5');
const TEAL = hexToBytes('6');
const WHITE = hexToBytes('7');
const OFF = hexToBytes('0');

// Conversion from hex to byte array
function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));

    return bytes;
}

export default class LED extends Component {
    constructor(props) {
        super(props);
    }

    // When the color buttons are clicked
    ledSwitch(type) {
        var message = eval(type);

        BleManager.write(deviceID, serviceIDled, charIDled, message)
            .then(() => {
                console.log('Write success');
            })
            .catch((error) => {
                console.log('Write  failed: ', error);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.center}>Click button to change sensor LED color</Text>
                <View style={styles.innerBox}>
                    <TouchableOpacity onPress={() => this.ledSwitch('OFF')}
                                      style={[{backgroundColor: 'rgb(20, 20, 20)'}, styles.button]}/>
                    <TouchableOpacity onPress={() => this.ledSwitch('WHITE')}
                                      style={[{backgroundColor: 'rgb(245, 245, 245)'}, styles.button]}/>
                </View>
                <View style={styles.innerBox}>
                    <TouchableOpacity onPress={() => this.ledSwitch('RED')}
                                      style={[{backgroundColor: 'rgb(255, 28, 35)'}, styles.button]}/>
                    <TouchableOpacity onPress={() => this.ledSwitch('GREEN')}
                                      style={[{backgroundColor: 'rgb(45, 237, 42)'}, styles.button]}/>
                </View>
                <View style={styles.innerBox}>
                    <TouchableOpacity onPress={() => this.ledSwitch('YELLOW')}
                                      style={[{backgroundColor: 'rgb(255, 246, 76)'}, styles.button]}/>
                    <TouchableOpacity onPress={() => this.ledSwitch('BLUE')}
                                      style={[{backgroundColor: 'rgb(43, 35, 255)'}, styles.button]}/>
                </View>
                <View style={styles.innerBox}>
                    <TouchableOpacity onPress={() => this.ledSwitch('PURPLE')}
                                      style={[{backgroundColor: 'rgb(249, 4, 233)'}, styles.button]}/>
                    <TouchableOpacity onPress={() => this.ledSwitch('TEAL')}
                                      style={[{backgroundColor: 'rgb(2, 255, 246)'}, styles.button]}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F5FCFF',
        alignItems: 'center',
        paddingTop: (Platform.OS === 'ios') ? 30 : 0,
    },
    center: {
        textAlign: 'center',
        fontWeight: '100',
        paddingBottom: 10,
    },
    innerBox: {
        flexDirection: 'row',
    },
    button: {
        width: width * 0.47,
        height: height * 0.21,
        borderRadius: 120,
    }
});

