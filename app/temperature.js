/* This is the temperature page of the SensorCloud
 * In this page, the user could get the temperature information by turning on the switch
 * The information being sent to the device will be displayed on the line graph
 * The graph will refresh once some data has been received
 * Note: the UUID for android and iOS could be different
 *
 * by: Philip Wang
 * on: June 22nd, 2017
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    NativeEventEmitter,
    NativeModules,
    Switch,
    Alert,
    Dimensions,
    Platform,
    TouchableOpacity,
    Button,
} from 'react-native';


import moment from 'moment';
import BleManager from 'react-native-ble-manager';
import SInfo from 'react-native-sensitive-info';
import Spinner from 'react-native-loading-spinner-overlay';
import Echarts from 'native-echarts';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const {width, height} = Dimensions.get('window');
import { StockLine } from 'react-native-pathjs-charts'

var counter = 1;
var timeOut;
var probes = [];
var Tdata = [[]];

// Service and UUID for temperature, for iOS only
var serviceIDtemp = "9D55A000-BC06-40EC-94CB-266609000419";
var charIDtemp = '9D55C000-BC06-40EC-94CB-266609000419';

// Conversation from unsigned 8 bit integer to signed
function convertExponent(value) {
    var int8 = new Int8Array(1);
    int8[0] = value;

    return int8[0];
}

// Convert the temperature
function convertTemp(firstValue, secondValue) {
    var finalValue = firstValue << 8 | secondValue;
    var int16 = new Int16Array(1);
    int16[0] = finalValue;

    return int16[0];
}

export default class Temperature extends Component {
    constructor(props) {
        super(props);
        this.state = {
            temperature: NaN,
            switch: false,
            visible: false,
            data: [],
            showGraph: false,
        };
        // Init the bluetooth
        BleManager.start({showAlert: false});
        // Check whether the user has paired with a probe or not
        this.checkSavedBluetooth();
    }

    componentDidMount() {
        bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', (data) => {
            for (var i = 0; i < probes.length; i++) {
                if (probes[i] === data.id) {
                    return;
                }
            }
            probes.push(data.id);
        });

        // Fired when the 3 seconds scan is finished
        bleManagerEmitter.addListener('BleManagerStopScan', () => {
            if (probes.length === 0) {
                this.setState({visible: false});
                setTimeout(() => {
                    Alert.alert(
                        'ERROR',
                        'Sorry, we cannot find any devices',
                        [
                            {text: 'Retry', onPress: () => this.startScan()},
                            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ],
                        {cancelable: false}
                    )
                }, 100);
            } else if (probes.length > 1) { // If more than 1 probe is found
                this.setState({visible: false});
                probes = [];
                setTimeout(() => {
                    Alert.alert(
                        'WARNING',
                        'Too many devices around. Please turn some off',
                        [
                            {text: 'Retry', onPress: () => this.startScan()},
                            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ],
                        {cancelable: false}
                    )
                }, 100);
            } else {
                // When there is exactly 1 active probe
                // console.log("here");
                // clearTimeout(timeOut);
                global.deviceID = probes[0];
                // Connect to the probe
                this.connectSensor();
            }
        });

        // Fired when the device hear the notification from the probe
        bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', (data) => {
            // Change the UUID if the platform is not iOS
            if (Platform.OS !== 'ios') {
                charIDtemp = charIDtemp.toLocaleLowerCase();
            }

            // Only update when it's the notification from the correct UUID
            if (data.characteristic === charIDtemp) {
                var exponent = convertExponent(data.value[4]);
                var temperature = convertTemp(data.value[2], data.value[1]);
                // Format the temperature
                temperature = temperature * Math.pow(10, exponent);
                temperature = temperature.toFixed(2);
                this.setState({temperature: temperature});
                // Add data to the graph array
                var tempArray = this.state.data;
                tempArray.push(temperature);

                Tdata[0].push({'x': counter, 'y': temperature});
                counter++;
                this.setState({data: tempArray});
            }
        });

        // Fired when a probe is connected
        // TODO: Check this funtion to make sure the loading animation could disappear
        bleManagerEmitter.addListener('BleManagerConnectPeripheral', (data) => {
            this.setState({visible: false});
            clearTimeout(timeOut);
            setTimeout(() => {
                Alert.alert(
                    'SUCCESS',
                    'Your probe has successfully connected to the phone',
                    [
                        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    ],
                    {cancelable: false}
                );
            }, 100);
            // Save the probe information
            SInfo.setItem('probe', global.deviceID, {
                sharedPreferencesName: 'probe',
                keychainService: 'probe'
            });
        });

        // Disconnected
        bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', (data) => {
            if (probes[0] != undefined) {
                setTimeout(() => {
                    alert('Your device has been disconnected');
                }, 100);
            }
        });
    };

    // Check if a probe has been paired before
    checkSavedBluetooth = () => {
        SInfo.getItem('probe', {
            sharedPreferencesName: 'probe',
            keychainService: 'probe'
        }).then((probe) => {
            if (probe != undefined) {
                this.setState({visible: true});
                probes[0] = probe;
                global.deviceID = probe;
                this.connectSensor();
            } else {
                this.startScan();
            }
        });
    };

    // Start scanning the probes around
    startScan() {
        this.setState({visible: true});
        BleManager.scan([serviceIDtemp], 3, true)
            .then((result) => {
            });
    }

    // Connect to the probe
    connectSensor = () => {
        BleManager.connect(global.deviceID)
            .then(() => {
                console.log('in Connect');
                BleManager.retrieveServices(global.deviceID)
                    .then((peripheralInfo) => {
                        console.log(peripheralInfo);
                    });
            })
            .catch((error) => {
                console.log("error");
                this.setState({visible: false});
                setTimeout(() => {
                    Alert.alert(
                        'ERROR',
                        'Device cannot be connected',
                        [
                            {
                                text: 'Reconnect', onPress: () => {
                                this.setState({visible: true});
                                this.connectSensor()
                            }
                            },
                            {
                                text: 'Rescan', onPress: () => {
                                clearTimeout(timeOut);
                                this.startScan()
                            }
                            },
                            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ],
                        {cancelable: false}
                    );
                }, 100)
            });
    };

    // When the switch has been pressed
    switchPressed = () => {
        if (this.state.switch === false) {
            this.setState({switch: true});
            this.startNotification();
        } else {
            this.setState({switch: false});
            this.stopNotification();
        }

    };

    // When the switch has been pressed
    graphSwitchPressed = () => {
        if (this.state.showGraph === false) {
            this.setState({showGraph: true});
        } else {
            this.setState({showGraph: false});
        }

    };

    startNotification() {
        BleManager.startNotification(global.deviceID, serviceIDtemp, charIDtemp)
            .then(() => {
                console.log('Notification started');
            })
            .catch((error) => {
                console.log('Notification error:', error);
            });
    }

    stopNotification() {
        this.setState({temperature: NaN});
        BleManager.stopNotification(global.deviceID, serviceIDtemp, charIDtemp)
            .then(() => {
                console.log('stopNotification success!');
            })
            .catch((error) => {
                console.log('stopNotification error:', error);
            });

        this.setState({temp: undefined});
    }

    stopScan() {
        BleManager.stopScan()
            .then(() => {
                console.log('Scan stopped');
            });
    }

    refreshGraph = () => {
        var originalArray = this.state.dataCombined;
        var newArray = this.state.data;
        var combinedArray = originalArray.concat(newArray);
        this.setState({dataCombined: combinedArray});
    };

    render() {
        let options = {
            width: width,
            height: 300,
            color: '#2980B9',
            margin: {
                top: 10,
                left: 35,
                bottom: 30,
                right: 10
            },
            animate: {
                type: 'delayed',
                duration: 200
            },
            axisX: {
                showAxis: true,
                showLines: true,
                showLabels: true,
                showTicks: true,
                zeroAxis: false,
                orient: 'bottom',
                tickValues: [],
                labelFunction: ((v) => {
                    let d = moment('2016-10-08 14:00', 'YYYY-MM-DD HH:mm')
                    return d.add((v * 2), 'hours').format('h:mm A')
                }),
                label: {
                    fontFamily: 'Arial',
                    fontSize: 8,
                    fontWeight: true,
                    fill: '#34495E'
                }
            },
            axisY: {
                showAxis: true,
                showLines: true,
                showLabels: true,
                showTicks: true,
                zeroAxis: false,
                orient: 'left',
                tickValues: [],
                label: {
                    fontFamily: 'Arial',
                    fontSize: 8,
                    fontWeight: true,
                    fill: '#34495E'
                }
            }
        };
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent={"SCANNING..."} textStyle={{color: '#FFF'}}/>
                <View style={styles.switchView}>
                    <View>
                        <Text style={styles.switchText}>Live Data</Text>
                        <Switch onValueChange={this.switchPressed} value={this.state.switch}/>
                    </View>
                    <View>
                        <Text style={styles.switchText}>Show Graph</Text>
                        <Switch onValueChange={this.graphSwitchPressed} value={this.state.showGraph}/>
                    </View>
                </View>
                <View>
                    { this.state.showGraph &&
                    <StockLine data={Tdata} options={options} xKey='x' yKey='y' />
                    }
                    { !this.state.showGraph &&
                    <Text style={{fontSize: 20, textAlign: 'center'}}>Toggle the switch to enable graph</Text>
                    }
                </View>
                <View style={styles.tempView}>
                    <Text style={styles.tempText}>{this.state.temperature}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: (Platform.OS === 'ios') ? 30 : 0,
        justifyContent: 'space-between',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    switchView: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: (Platform.OS === 'ios') ? 10 : 15,
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(212, 224, 206)'
    },
    switchText: {
        fontSize: 20,
    },
    tempView: {
        left: 0,
        right: 0,
        bottom: 0,
        width: width,
        height: 120,
        backgroundColor: 'rgb(212, 224, 206)',
    },
    tempText: {
        textAlign: 'center',
        fontSize: 70,
        paddingTop: 20
    },
    button: {
        height: 30,
        paddingLeft: 5,
        backgroundColor: 'rgb(191, 244, 66)',
    },
});

