/**
 * Created by philipwang on 6/20/17.
 */
import React from 'react';
import {
    TabNavigator,
} from 'react-navigation';

import Temperature from './temperature'
import LED from './LED';
import Battery from './battery';
import {Icon} from 'react-native-elements'


const Navigator = TabNavigator({
    Temperature: {
        screen: Temperature,
        navigationOptions: {
            tabBarLabel: 'Temperature',
            tabBarIcon: ({tintColor}) => <Icon name="brightness-2" size={35} color={tintColor}/>
        }
    },
    LED: {
        screen: LED,
        navigationOptions: {
            tabBarLabel: 'LED',
            tabBarIcon: ({tintColor}) => <Icon name="highlight" size={35} color={tintColor}/>
        },
    },
    Battery: {
        screen: Battery,
        navigationOptions: {
            tabBarLabel: 'Battery',
            tabBarIcon: ({tintColor}) => <Icon name="battery-full" size={35} color={tintColor}/>
        },
    },
})
;

export default Navigator;

